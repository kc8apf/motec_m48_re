#!/usr/local/bin/python3

import struct
import sys

# Log record format
# Little-endian unless otherwise noted
# Byte  Description         Data Type               Unit    Scale       Offset
#  0    RPM                 2-byte unsigned int     RPM     1           0
#  2    Fuel Used           2-byte unsigned int     ???     0.1         0
#  4    Speed 1             2-byte unsigned int             1           0
#  6    Speed 2             2-byte unsigned int             1           0
#  8    Aux Volt            2-byte unsigned int     V       0.1         0
# 10    Aux Temp            2-byte unsigned int     ºC      0.1         0
# 12    MAP                 2-byte unsigned int     kPa     0.1         0
# 14    Slip                1-byte unsigned int             1           0
# 15    Throttle Position   1-byte unsigned int     %       0.5         0
# 16    Lambda              1-byte unsigned int             0.01        0
# 17    Engine Temp         1-byte unsigned int     ºC      1           -50
# 18    AirT                1-byte unsigned int     ºC      1           -50
# 19    Battery Voltage     1-byte unsigned int     V       0.1         0
# 20    EcuT                1-byte unsigned int     ºC      1           -50
# 21    Fapw                1-byte unsigned int     ms?     0.1         0
# 22    Fepw                1-byte unsigned int     ms?     0.1         0
# 23    Fuel Timing         1-byte unsigned int     ºBTDC   5           0
# 24    Duty                1-byte unsigned int             1           0
# 25    LaST                1-byte unsigned int             0.1         0
# 26    Accl Trim?          1-byte unsigned int             0.1         0
# 27    Ign. Adv.           1-byte unsigned int     ºBTDC   0.5         0
# 28    Load Point          1-byte unsigned int             2           0
# 29    Eff. Point          1-byte unsigned int             2           0
# 30    PWM1                1-byte unsigned int     %       1           0
# 31    PWM2                1-byte unsigned int     %       1           0
# 32    PWM3                1-byte unsigned int     %       1           0
# 33    PWM4                1-byte unsigned int     %       1           0
# 34    Errors              4-byte bit field
#   0   Throttle Position
#   1   Map Sensor
#   2   Air Temp
#   3   Engine Temp
#   4   Battery
#   5   Lambda
#   6   Aux Volt
#   7   Aux Temp
#   8   ECU Temp
#   9   Delta Batt
#  10   E2
#  11   E3
#  12   E4
#  13   E5
#  14   E6
#  15   E7
#  16   Injector 1
#  17   Injector 2
#  18   Injector 3
#  19   Injector 4
#  20   Injector 5
#  21   Injector 6
#  22   Injector 7
#  23   Injector 8
#  24   Low Batt
#  25   Over Boost
#  26   No Sync
#  27   Sync Error
#  28   No Ref
#  29   Ref Error
#  30   RPM Limit
#  31   Max Injector Duty
# 36    Gear              upper nibble unsigned int         1           0
# 37    SyncP               1-byte unsigned int             1           0
# 38    FAuT                1-byte unsigned int             1           0
# 39    FAuV                1-byte unsigned int             1           0
# 40    LaLT                1-byte unsigned int             0.1         0
# 41    ICut                1-byte unsigned int             1           0
# 42    FCut                1-byte unsigned int             1           0
# 44    Timestamp           4-byte?                         ?           ?

# When RPM is negative, it contains metadata.
# -2: Comment
# -3: Car/Venue
# -4: ECU Serial #?
# -7: Tuning info.
#      Bytes 6-7 of record specify number of bytes following this record are
#      tuning data.

class CarVenueMetadata:
  def __init__(self, record_data, fp):
    (_, self.car, self.venue, _) = struct.unpack("<h10s10s15s14x", record_data)
    self.car = str(self.car, 'ascii').strip()
    self.venue = str(self.venue, 'ascii').strip()

  def __str__(self):
    return "Car: \"{}\" Venue: \"{}\"".format(self.car, self.venue)


class CommentMetadata:
  def __init__(self, record_data, fp):
    (_, self.comment,) = struct.unpack("<h36s13x", record_data)
    self.comment = str(self.comment, 'ascii').strip()

  def __str__(self):
    return "Comment: \"{}\"".format(self.comment)


class TuneMetadata:
  def __init__(self, record_data, fp):
    (_, self.tune_length) = struct.unpack("<h4xH43x", record_data)
    self.tune_data = fp.read(self.tune_length)

  def __str__(self):
    return "Tuning data ({} bytes)".format(self.tune_length)


class EcuSerialMetadata:
  def __init__(self, record_data, fp):
    (_, self.serial_number,) = struct.unpack("<h49s", record_data)
    self.serial_number = str(self.serial_number, 'ascii').strip()

  def __str__(self):
    return "Serial Number: {}".format(self.serial_number)


class UnknownMetadata:
  def __init__(self, record_data, fp):
    (self.tag, self.data) = struct.unpack("<h49s", record_data)
    
  def __str__(self):
    fancy_hex = '\n\t'.join([self.data[i:i+10].hex() for i in range(0, len(self.data), 10)])
    return "Unknown Metadata Tag {}:\n\t{}".format(self.tag, fancy_hex)


METADATA_DECODERS = {
    -7: TuneMetadata,
    -4: EcuSerialMetadata,
    -3: CarVenueMetadata,
    -2: CommentMetadata,
}

class Record:
  def __init__(self, record_index, record_data):
    self.line = record_index
    (self.rpm, self.fuel_usage, self.aux_volt,
     self.manifold_absolute_pressure,
     self.exhaust_lambda,
     self.engine_temp, self.air_temp,
     self.battery_voltage, self.ecu_temp,
     self.fuel_timing, self.ignition_advance,
     self.load_point, self.eff_point,
     self.pwm1_percent, self.pwm4_percent) = struct.unpack(
        "<hB5xh2xh2xBBBBB2xB3xBBBB2xB17x", record_data)

  def __str__(self):
    return "\n".join([
        "Record:",
        "  Line: {}".format(self.line),
        "  RPM: {}".format(self.rpm),
        "  Fuel Usage: {}".format(self.fuel_usage * 0.1),
        "  Aux Volt: {}V".format(self.aux_volt * 0.1),
        "  MAP: {}kPa".format(self.manifold_absolute_pressure * 0.1),
        "  Lambda: {}".format(self.exhaust_lambda * 0.01),
        "  Engine Temp: {}ºC".format(self.engine_temp - 50),
        "  Air Temp: {}ºC".format(self.air_temp - 50),
        "  ECU Temp: {}ºC".format(self.ecu_temp - 50),
        "  Fuel Timing: {}ºBTDC".format(self.fuel_timing * 5),
        "  Ign. Adv.: {}ºBTDC".format(self.ignition_advance * 0.5),
        "  Battery Voltage: {}V".format(self.battery_voltage * 0.1),
        "  Load Point: {}".format(self.load_point * 2),
        "  Eff. Point: {}".format(self.eff_point * 2),
        "  PWM1: {}%".format(self.pwm1_percent),
        "  PWM4: {}%".format(self.pwm4_percent),
        ])

if len(sys.argv) < 2:
  print("Usage: {} <file>".format(sys.argv[0]))
  sys.exit(-1)

try:
  with open(sys.argv[1], "rb") as f:
    record_index = 1

    while True:
      record_raw = f.read(51)
      if not record_raw:
        break

      (rpm,) = struct.unpack("<h", record_raw[0:2])
      if (rpm < 0):
        metadata_decoder = METADATA_DECODERS.get(rpm, UnknownMetadata)
        print(metadata_decoder(record_raw, f))
      else:
        # Decode normal record.
        print(Record(record_index, record_raw))
        record_index = record_index + 1
except OSError as e:
  print(e)
