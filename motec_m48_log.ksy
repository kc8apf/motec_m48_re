meta:
  id: motec_m48_log
  file-extension: log
  endian: le
  encoding: ASCII
seq:
  - id: record
    type: log_record
    repeat: eos
types:
  log_record:
    seq:
      - id: rpm
        type: s2
      - id: record
        type:
          switch-on: rpm
          cases:
            -16: log_header
            -7: log_tune
            -4: log_serial_no
            -3: log_car
            -2: log_comment
            _: log_set
  log_comment:
    seq:
      - id: comment
        size: 49
        type: strz
  log_car:
    seq:
      - id: car
        size: 49
        type: strz
  log_serial_no:
    seq:
      - id: serial_no
        size: 49
        type: strz
  log_tune:
    seq:
      - id: unknown
        size: 4
      - id: data_len
        type: u2
      - id: data
        size: data_len
      - id: padding
        size: 51 - ((8 + data_len) % 51)
  log_header:
    seq:
      - id: data
        size: 49
  log_set:
    seq:
      - id: fuel_used_raw
        type: u2
      - id: speed_1
        type: u2
      - id: speed_2
        type: u2
      - id: aux_volt_raw
        type: u2
      - id: aux_temp_raw
        type: u2
      - id: manifold_air_pressure_raw
        type: u2
      - id: slip
        type: u1
      - id: throttle_position_raw
        type: u1
      - id: lambda_raw
        type: u1
      - id: engine_temp_raw
        type: u1
      - id: air_temp_raw
        type: u1
      - id: battery_voltage_raw
        type: u1
      - id: ecu_temp_raw
        type: u1
      - id: fuel_actual_pulse_width_raw
        type: u1
      - id: fuel_expected_pulse_width_raw
        type: u1
      - id: fuel_timing_raw
        type: u1
      - id: duty
        type: u1
      - id: last_raw
        type: u1
      - id: accel_trim_raw
        type: u1
      - id: ignition_advance_raw
        type: u1
      - id: load_point_raw
        type: u1
      - id: efficiency_point_raw
        type: u1
      - id: pwm1
        type: u1
      - id: pwm2
        type: u1
      - id: pwm3
        type: u1
      - id: pwm4
        type: u1
      - id: err_throttle_position
        type: b1
      - id: err_map_sesnor
        type: b1
      - id: err_air_temp
        type: b1
      - id: err_engine_temp
        type: b1
      - id: err_battery
        type: b1
      - id: err_lambda
        type: b1
      - id: err_aux_volt
        type: b1
      - id: err_aux_temp
        type: b1
      - id: err_ecu_temp
        type: b1
      - id: err_delta_batt
        type: b1
      - id: err_e2
        type: b1
      - id: err_e3
        type: b1
      - id: err_e4
        type: b1
      - id: err_e5
        type: b1
      - id: err_e6
        type: b1
      - id: err_e7
        type: b1
      - id: err_injector
        type: b1
        repeat: expr
        repeat-expr: 8
      - id: err_low_bat
        type: b1
      - id: err_over_boost
        type: b1
      - id: err_no_sync
        type: b1
      - id: err_sync_error
        type: b1
      - id: err_no_ref
        type: b1
      - id: err_ref_error
        type: b1
      - id: err_rpm_limit
        type: b1
      - id: err_max_injector_duty
        type: b1
      - id: gear
        type: u1
      - id: sync_position
        type: u1
      - id: faut
        type: u1
      - id: fauv
        type: u1
      - id: lalt_raw
        type: u1
      - id: icut
        type: u1
      - id: fcut
        type: u1
      - id: unknown
        size: 1
      - id: sets_per_second
        type: u1
        doc: "Log was recorded at 10 sets per second....?"
      - id: timestamp
        type: u4
        doc: "Maybe data set #"
    instances:
      fuel_used:
        value: fuel_used_raw * 0.1
      aux_volt:
        value: aux_volt_raw * 0.1
        doc: "Volts"
      aux_temp:
        value: aux_temp_raw * 0.1
        doc: "ºC"
      manifold_air_pressure:
        value: manifold_air_pressure_raw * 0.1
        doc: "kPa"
      throttle_position:
        value: throttle_position_raw * 0.5
        doc: "%"
      lambda:
        value: lambda_raw * 0.01
      engine_temp:
        value: engine_temp_raw - 50
        doc: "ºC"
      air_temp:
        value: air_temp_raw - 50
        doc: "ºC"
      battery_voltage:
        value: battery_voltage_raw * 0.1
        doc: "Volts"
      ecu_temp:
        value: ecu_temp_raw - 50
        doc: "ºC"
      fuel_actual_pulse_width:
        value: fuel_actual_pulse_width_raw * 0.1
        doc: "ms?"
      fuel_expected_pulse_width:
        value: fuel_expected_pulse_width_raw * 0.1
        doc: "ms?"
      fuel_timing:
        value: fuel_timing_raw * 5
        doc: "ºBTDC"
      last:
        value: last_raw * 0.1
      accel_trim:
        value: accel_trim_raw * 0.1
      ignition_advance:
        value: ignition_advance_raw * 0.5
        doc: "ºBTDC"
      load_point:
        value: load_point_raw * 2
      efficiency_point:
        value: efficiency_point_raw * 2
      lalt:
        value: lalt_raw * 0.1